# expmanager
This is a personal expense manager web app which makes use of bootstrap for styling and PHP for backend. It also uses Charts.js API to generate graphs and stores data in SQL based PHPMyAdmin

Dummy data has already been added to the database.
A day's total data is updated to the database once the date changes not before that, so the updated graph won't be visible until the date changes.
