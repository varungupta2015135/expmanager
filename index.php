<?php
	require('includes/dbconfig.php');
	include('includes/header.php');
?>

<?php
	$result=mysqli_query($con, "select * from daily");
	while($row=mysqli_fetch_assoc($result)){
		$date=$row['date'];
	}
	$lastSaveMonth=date_format(new DateTime($date), 'M');
	$lastSaveDate=date_format(new DateTime($date), 'd');
	$nowMonth=date_format(new DateTime('now'), 'M');
	$nowDate=date_format(new DateTime('now'), 'd');
	if($nowDate!=$lastSaveDate && $nowMonth==$lastSaveMonth){
		$result=mysqli_query($con, "select * from daily");
		while($row=mysqli_fetch_assoc($result)){
			$totalGrocery+=$row['grocery'];
			$totalEntertainment+=$row['entertainment'];
			$totalFood+=$row['food'];
			$totalTravel+=$row['travel'];
			$totalMisc+=$row['misc'];
		}
		$result=mysqli_query($con, "insert into $nowMonth (grocery, entertainment, food, travel, misc) values('$totalGrocery', '$totalEntertainment', '$totalFood', '$totalTravel', $totalMisc)");
		mysqli_query($con, "truncate table daily");
	}
	else if($nowMonth!=$lastSaveMonth){
		$result=mysqli_query($con, "select * from $lastSaveMonth");
		while($row=mysqli_fetch_assoc($result)){
			$totalGrocery+=$row['grocery'];
			$totalEntertainment+=$row['entertainment'];
			$totalFood+=$row['food'];
			$totalTravel+=$row['travel'];
			$totalMisc+=$row['misc'];
		}
		$result=mysqli_query($con, "insert into $nowMonth (grocery, entertainment, food, travel, misc) values('$totalGrocery', '$totalEntertainment', '$totalFood', '$totalTravel', $totalMisc)");
		mysqli_query($con, "truncate table $nowMonth");
	}
?>
<h2>Welcome</h2>
<div id="para1"></div>
<hr class="basic">

<h4>Today's Expenses</h4>
<div class="row">
	<!-- Grocery-->
	<div class="col-md-6 col-sm-6">
		<div class="panel panel-primary">
			<div class="panel-heading">Grocery</div>
			<div class="panel-body">
				<form method="post">
					<div class="input-group">
			  			<span class="input-group-addon">₹</span>
			  			<input type="number" class="form-control" id="setGroceryAmount" aria-label="Amount" placeholder="Money spent" name="grocery" min="1">
			  			<span class="input-group-btn">
			    			<button class="btn btn-default" id="addGrocery" type="submit" name="submitGrocery">Add</button>
			  			</span>
					</div>
				</form>
				<?php
					if(isset($_POST['submitGrocery'])){
						$data=$_POST['grocery'];
						$sql="insert into daily (grocery) values('$data')";
						if(mysqli_query($con, $sql)){
							?>
							<div class="alert alert-success" style="margin-top: 5px; margin-bottom: -8px;" id="setAlertGrocery" role="alert">Added</div>
							<?php
						}
					}
				?>
				
			</div>
		</div>
	</div>
	<!-- Entertainment-->
	<div class="col-md-6 col-sm-6">
		<div class="panel panel-success">
			<div class="panel-heading">Entertainment</div>
			<div class="panel-body">
				<form method="post">
					<div class="input-group">
			  			<span class="input-group-addon">₹</span>
			  			<input type="number" class="form-control" name="entertainment" id="setEntertainmentAmount" aria-label="Amount" placeholder="Money spent" min="1">
			  			<span class="input-group-btn">
			    			<button class="btn btn-default" id="addEntertainment" name="submitEntertainment" type="submit">Add</button>
			  			</span>
					</div>
				</form>
				<?php
					if(isset($_POST['submitEntertainment'])){
						$data=$_POST['entertainment'];
						$sql="insert into daily (entertainment) values('$data')";
						if(mysqli_query($con, $sql)){
							?>
							<div class="alert alert-success" style="margin-top: 5px; margin-bottom: -8px;" id="setAlertEntertainment" role="alert">Added</div>
							<?php
						}
					}
				?>
				
			</div>
		</div>
	</div>
</div>
<hr class="style-seven">
	<!-- Food -->
<div class="row">
	<div class="col-md-6 col-sm-6">
		<div class="panel panel-info">
			<div class="panel-heading">Food</div>
			<div class="panel-body">
				<form method="post">
					<div class="input-group">
			  			<span class="input-group-addon">₹</span>
			  			<input type="number" class="form-control" name="food" id="setFoodAmount" aria-label="Amount" placeholder="Money spent" min="1">
			  			<span class="input-group-btn">
			    			<button class="btn btn-default" name="submitFood" id="addFood" type="submit">Add</button>
			  			</span>
					</div>
				</form>
				<?php
					if(isset($_POST['submitFood'])){
						$data=$_POST['food'];
						$sql="insert into daily (food) values('$data')";
						if(mysqli_query($con, $sql)){
							?>
							<div class="alert alert-success" style="margin-top: 5px; margin-bottom: -8px;" id="setAlertFood" role="alert">Added</div>
							<?php
						}
					}
				?>
			</div>
		</div>
	</div>
	<!-- Travel-->
	<div class="col-md-6 col-sm-6">
		<div class="panel panel-warning">
			<div class="panel-heading">Travel</div>
			<div class="panel-body">
				<form method="post">
					<div class="input-group">
			  			<span class="input-group-addon">₹</span>
			  			<input type="number" class="form-control" id="setTravelAmount" name="travel" aria-label="Amount" placeholder="Money spent" min="1">
			  			<span class="input-group-btn">
			    			<button class="btn btn-default" id="addTravel" name="submitTravel" type="submit">Add</button>
			  			</span>
					</div>
				</form>
				<?php
					if(isset($_POST['submitTravel'])){
						$data=$_POST['travel'];
						$sql="insert into daily (travel) values('$data')";
						if(mysqli_query($con, $sql)){
							?>
							<div class="alert alert-success" style="margin-top: 5px; margin-bottom: -8px;" id="setAlertTravel" role="alert">Added</div>
							<?php
						}
					}
				?>
			</div>
		</div>
	</div>
</div>
<hr class="style-seven">
	<!-- Miscellaneous-->
<div class="row">
	<div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
		<div class="panel panel-danger">
			<div class="panel-heading">Miscellaneous</div>
			<div class="panel-body">
				<form method="post">
					<div class="input-group">
			  			<span class="input-group-addon">₹</span>
			  			<input type="number" class="form-control" name="misc" id="setMiscAmount" aria-label="Amount" placeholder="Money spent" min="1">
			  			<span class="input-group-btn">
			    			<button class="btn btn-default" id="addMisc" name="submitMisc" type="submit">Add</button>
			  			</span>
					</div>
				</form>
				<?php
					if(isset($_POST['submitMisc'])){
						$data=$_POST['misc'];
						$sql="insert into daily (misc) values('$data')";
						if(mysqli_query($con, $sql)){
							?>
							<div class="alert alert-success" style="margin-top: 5px; margin-bottom: -8px;" id="setAlertMisc" role="alert">Added</div>
							<?php
						}
					}
				?>
			</div>
		</div>
	</div>
</div>

<?php
    include('includes/footer.php');
?>

