<?php
	require('includes/dbconfig.php');
	include('includes/header.php');
?>

<div class="col-md-6 col-sm-12 col-xs-12">
	<canvas id="myChart1" height="60%" width="80%"></canvas>
</div>
<div class="col-md-6 col-sm-12 col-xs-12">
	<canvas id="myChart2" height="60%" width="80%"></canvas>
</div>

<?php
		$totalGrocery=0;
		$totalEntertainment=0;
		$totalFood=0;
		$totalTravel=0;
		$totalMisc=0;
		$total=0;

	$sql="select * from daily";
	$result=mysqli_query($con, $sql);
	while($row=mysqli_fetch_assoc($result)){
		$totalGrocery+=$row['grocery'];
		$totalEntertainment+=$row['entertainment'];
		$totalFood+=$row['food'];
		$totalTravel+=$row['travel'];
		$totalMisc+=$row['misc'];
		$date=$row['date'];
	}
	$dataArray=array($totalGrocery, $totalEntertainment, $totalFood, $totalTravel, $totalMisc);
?>

<?php
	require('includes/footer.php');
?>