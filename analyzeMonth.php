<?php
	require('includes/dbconfig.php');
	include('includes/header.php');
?>

<div class="col-md-12">
	<canvas id="myChart3" height="35%" width="50%"></canvas>
</div>

<?php
		$totalGrocery=0;
		$totalEntertainment=0;
		$totalFood=0;
		$totalTravel=0;
		$totalMisc=0;
		$total=0;

	$sql="select * from Mar";
	$result=mysqli_query($con, $sql);
	while($row=mysqli_fetch_assoc($result)){
		$totalGrocery+=$row['grocery'];
		$totalEntertainment+=$row['entertainment'];
		$totalFood+=$row['food'];
		$totalTravel+=$row['travel'];
		$totalMisc+=$row['misc'];
	}
	$dataArray=array($totalGrocery, $totalEntertainment, $totalFood, $totalTravel, $totalMisc);
?>

<?php
	require('includes/footer.php');
?>