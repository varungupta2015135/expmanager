<nav class="navbar navbar-default navbar-inverse" style="margin-bottom: 50px;">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                  </button>
                    <a class="navbar-brand" href="index.php">ExpManage</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav">
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Analyze <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="analyzeToday.php">Today</a></li>
                        <li><a href="analyzeMonth.php">Monthly</a></li>
                      </ul>
                    </li>
                  </ul>
                  <ul class="nav navbar-nav navbar-right">
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>