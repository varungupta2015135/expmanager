<script>

document.getElementById("para1").innerHTML = formatAMPM();

function formatAMPM() {
var d = new Date(),
    minutes = d.getMinutes().toString().length == 1 ? '0'+d.getMinutes() : d.getMinutes(),
    hours = d.getHours().toString().length == 1 ? '0'+d.getHours() : d.getHours(),
    ampm = d.getHours() >= 12 ? 'pm' : 'am',
    months = ['January','February','March','April','May','June','July','August','September','October','November','December'],
    days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
return "It's " + days[d.getDay()]+' '+months[d.getMonth()]+' '+d.getDate()+', '+d.getFullYear()+ " today"
}

</script>
<script>

$("#setGroceryAmount").click(function(){
	$("#setAlertGrocery").css("display", "none");
})


$("#setEntertainmentAmount").click(function(){
	$("#setAlertEntertainment").css("display", "none");
})

$("#setFoodAmount").click(function(){
	$("#setAlertFood").css("display", "none");
})

$("#setTravelAmount").click(function(){
	$("#setAlertTravel").css("display", "none");
})

$("#setMiscAmount").click(function(){
	$("#setAlertMisc").css("display", "none");
})

</script>

<script>
	Chart.defaults.global.defaultFontColor='black';
var ctx1 = document.getElementById("myChart1").getContext('2d');
var myChart1 = new Chart(ctx1, {
    type: 'bar',
    data: {
        labels: ["Grocery", "Entertainment", "Food", "Travel", "Miscellaneous"],
        datasets: [{
            label: 'Money Spent in ₹',
            data: <?= json_encode($dataArray); ?>,
            backgroundColor: [
                'rgba(255, 99, 132, 0.7)',
                'rgba(54, 162, 235, 0.7)',
                'rgba(255, 206, 86, 0.7)',
                'rgba(75, 192, 192, 0.7)',
                'rgba(153, 102, 255, 0.7)',
                'rgba(255, 159, 64, 0.7)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
            	gridLines: {
            		color: "rgba(0, 0, 0, 0.2)"
            	},
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
<script>
var ctx2 = document.getElementById("myChart2").getContext('2d');
var myChart2 = new Chart(ctx2, {
    type: 'pie',
    data: {
        labels: ["Grocery", "Entertainment", "Food", "Travel", "Miscellaneous"],
        datasets: [{
            label: 'Money Spent in ₹',
            data: <?= json_encode($dataArray); ?>,
            backgroundColor: [
                'rgba(255, 99, 132, 0.7)',
                'rgba(54, 162, 235, 0.7)',
                'rgba(255, 206, 86, 0.7)',
                'rgba(75, 192, 192, 0.7)',
                'rgba(153, 102, 255, 0.7)',
                'rgba(255, 159, 64, 0.7)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    }
});
</script>
<script>
var ctx3 = document.getElementById("myChart3").getContext('2d');
var myChart3 = new Chart(ctx3, {
    type: 'radar',
    data: {
        labels: ["Grocery", "Entertainment", "Food", "Travel", "Miscellaneous"],
        datasets: [
        	{
        		label: "Money spent in March",
        		backgroundColor: 'rgba(54, 162, 235, 0.5)',
        		borderColor: 'rgba(54, 162, 235, 1)',
        		borderWidth: 2,
        		data: <?= json_encode($dataArray); ?>
        	},
        	{
        		label: 'Money spent in April',
        		backgroundColor: 'rgba(177, 23, 234, 0.5)',
        		borderColor: 'rgba(177, 23, 234, 1)',
        		borderWidth: 2,
        		data: [1200, 1500, 2000, 1200, 3500]
        	}
        ]
    },
    options: {
        scales: {
           	gridLines: {
            	color: "rgba(0, 0, 0, 0.8)"
            }
        }
    }
});

</script>

</div>
</div>
</body>
</html>